class Course
  attr_reader :name, :department, :credits, :students, :days, :time_block

  def initialize(name, department, credits, days, time_block)
    @name = name
    @department = department
    @credits = credits
    @students = []
    @days = days
    @time_block = time_block
  end

  def add_student(student)
    student.enroll(self)
  end

  def conflicts_with?(course)
    self.days.each do |block|
      if course.days.include?(block) && self.time_block == course.time_block
        return true
      end
    end
    false
  end
end
