class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    @courses.each do |course|
      raise "nonono" if course.conflicts_with?(new_course)
    end
    @courses << new_course unless @courses.include?(new_course)
    new_course.students << self
  end

  def course_load
    dep_creds = Hash.new(0)

    @courses.each do |course|
      dep_creds[course.department] += course.credits
    end

    dep_creds
  end
end
